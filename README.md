# README #
Stock Trader project to work with all the important things/concepts learned on vue

### What will i learn? ###

* Vuex
* Router
* spread operators
* components (childs and parents)
* Firebase (CRUD)
* Animations
* Vue developer tools (debug)

### Installation ###
* For using spread operators(...): npm i --save-dev babel-preset-stage-2
* Vue router: npm i --save vue-router
* Vuex: https://vuex.vuejs.org/installation.html