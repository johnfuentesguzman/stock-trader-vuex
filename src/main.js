import Vue from 'vue'
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';

import App from './App.vue'
import { routes } from './routes';
import store from './store/store';

Vue.use(VueRouter);
Vue.use(VueResource);

Vue.http.options.root = 'https://vuex-stock-trader-a53e4.firebaseio.com/'; // http host

Vue.filter('currency', (value) => { // filter to set the UI pipe, used in  header.vue and home.vue page
  return '$' + value.toLocaleString();
});

const router = new VueRouter({ // always after Vue.use(VueRouter);
  mode: 'history', // to avoid to have to set hashtag (#) in url
  routes
});

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
