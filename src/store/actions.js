import Vue from 'vue';

// using vuew resource
// this is action for that reason im using a mutation call here, cause the mutation must syncronic and at that point i already have the data from firebase
export const loadData = ({commit}) => {
    Vue.http.get('data.json')
        .then(response => response.json())
        .then(data => {
            if (data) {
                const stocks = data.stocks;
                const funds = data.funds;
                const stockPortfolio = data.stockPortfolio;

                const portfolio = {
                    stockPortfolio,
                    funds
                };

                commit('SET_STOCKS', stocks); //calling mutation directly
                commit('SET_PORTFOLIO', portfolio);
            }
        });
};