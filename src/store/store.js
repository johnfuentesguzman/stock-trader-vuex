import Vue from 'vue';
import Vuex from 'vuex';

import stocks from './modules/Stocks-State';
import portfolio from './modules/Portfolio-State';

import * as actions from './actions';

Vue.use(Vuex);

export default new Vuex.Store({
    actions,
    modules: {  // the actions, muatation and getters wich are created like this (exporting a method payload must be included in 'modules'  object )
        stocks,
        portfolio
    }
});